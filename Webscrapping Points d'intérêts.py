import requests
import matplotlib.pyplot as plt
import os
from bs4 import BeautifulSoup
from tqdm import tqdm
import pandas as pd
import csv
import math

page = requests.get("https://fr.wikipedia.org/wiki/Monuments_et_sites_de_Paris")
soup = BeautifulSoup(page.content, 'html.parser')

title = soup.select("h3")
liste = []
for i in range(len(title)):
    try:
        elt = title[i].find_next_sibling()
        while elt.name != 'ul':
            elt = elt.find_next_sibling()
        liste.append(elt)
    except:
        print('ok normalement')

data = []
for i in tqdm(range(len(liste))):
    temp = liste[i].find_all("li")
    links = [temp[i].a for i in range(len(temp))]
    L = []
    for a in links:
        try:
            href = a["href"]
        except:
            print("\nPas de lien")
        try:
            next_page = requests.get("https://fr.wikipedia.org" + href)
            next_soup = BeautifulSoup(next_page.content, 'html.parser')
            coord = next_soup.select('#coordinates')[0]
            coord = float(coord.a['data-lat']), float(coord.a['data-lon'])
            nom = next_soup.select('#firstHeading')[0].get_text()
            if [nom, coord] != []:
                L.append([nom, coord])
        except:
            print("\nPas de coord pour ", href)
            
    if L != []:
        data.append([title[i].get_text()[:-29], L])
        





page = requests.get("https://fr.wikipedia.org/wiki/Liste_des_mus%C3%A9es_de_Paris")
soup = BeautifulSoup(page.content, 'html.parser')

tableau = soup.select("tbody")
links = []
for t in tableau:
    t = t.select("tr")
    t = t[1:]
    for elt in t:
        links.append(elt.a)
        

title = soup.select("h3")
liste = []
for i in range(len(title)):
    try:
        elt = title[i].find_next_sibling()
        while elt.name != 'h3' or elt.name != 'div':
            if elt.name == 'ul':
                liste.append(elt)
            elt = elt.find_next_sibling()
            
    except:
        None


for i in range(len(liste)):
    temp = liste[i].find_all("li")
    for i in range(len(temp)):
        links.append(temp[i].a) 
        
links = links[:-28]
        
L = []
for a in tqdm(links):
    try:
        href = a["href"]
    except:
        print("\nPas de lien")
    try:
        next_page = requests.get("https://fr.wikipedia.org" + href)
        next_soup = BeautifulSoup(next_page.content, 'html.parser')
        coord = next_soup.select('#coordinates')[0]
        coord = float(coord.a['data-lat']), float(coord.a['data-lon'])
        nom = next_soup.select('#firstHeading')[0].get_text()
        if [nom, coord] != []:
            L.append([nom, coord])
    except:
        print("\nPas de coord pour ", href)
        

data2 = []
for i in range(len(data)):
    data2 += data[i][1]
    
totaldata = data2 + L

nom = []
lat = []
lon = []

for i in range(len(totaldata)):
    nom.append(totaldata[i][0])
    lat.append(totaldata[i][1][0])
    lon.append(totaldata[i][1][1])
    
df = pd.DataFrame(data = {'nom': nom, 'lat': lat, 'long': lon})

writer = pd.ExcelWriter('git/Olivier/coord.xlsx', engine='xlsxwriter')
df.to_excel(writer, 'Feuille 1')

writer.save()

import pandas as pd

file = 'git/Olivier/coord old.xlsx'
xl = pd.ExcelFile(file)

df = xl.parse('Feuille 1')
df['lat']

dejaVu = []
aSuppr = []
for i in range(len(df['lat'])):
    if ((df['lat'][i], df['long'][i]) not in dejaVu):
        dejaVu.append((df['lat'][i], df['long'][i]))
    else:
        aSuppr.append(i)


df = df.drop(df.index[aSuppr])


